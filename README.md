# swift

Compiled programming language developed by Apple Inc. https://swift.org

* [repology](https://repology.org/project/swift-lang/versions)

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=swiftlang&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

# Tutorials
* [Learn X in Y minutes, where X=swift](https://learnxinyminutes.com/docs/swift/)

# Try it online
* https://tio.run/#swift4

# Books
* *Mastering Swift 6 - Seventh Edition*
  2024-12? Jon Hoffman (Packt Publishing)
* *Swift Cookbook - Third Edition*
  2024-06 Keith Moon, Chris Barker, Daniel Bolella, Nathan Lawlor (Packt Publishing)
* *Swift in Depth*
  2018-12 Tjeerd in 't Veen (Manning Publications)
* *Hands-On Design Patterns with Swift*
2018-12 Florent Vilmart, Giordano Scalzo, Sergio De Simone (Packt Publishing)
* *Hands-On Server-Side Web Development with Swift*
  2018-11 Angus Yeung (Packt Publishing)
* [*Functional Programming: A PragPub Anthology: Exploring Clojure, Elixir, Haskell, Scala, and Swift*
  ](https://pragprog.com/titles/ppanth/functional-programming-a-pragpub-anthology/)
  2017-07 Michael Swaine and the PragPub writers
* [*Swift functional programming*
  ](https://www.worldcat.org/search?q=ti%3ASwift+functional+programming)
  2017-04 Fatih Nayebi (Packt Publishing)

## Functional programming
* [*Swift Functors, Applicatives, and Monads in Pictures*
  ](http://www.mokacoding.com/blog/functor-applicative-monads-in-pictures/)
  2015

# Features
## Memory management
### Automatic Reference Counting
* [*Automatic Reference Counting*
  ](https://docs.swift.org/swift-book/LanguageGuide/AutomaticReferenceCounting.html)
* [*Deinitialization*
  ](https://docs.swift.org/swift-book/LanguageGuide/Deinitialization.html)

## Error handling
* [*Error-Handling in Swift-Language*
  ](https://stackoverflow.com/questions/24010569/error-handling-in-swift-language/26749528#26749528)
  (2017)

## Functional programming
...

# Operators
* swift custom operators
  [DDG](https://duckduckgo.com/?q=swift+custom+operators)

# Compared to other languages
* [*Programming Languages: What are the differences between Rust and Swift?*
  ](https://www.quora.com/Programming-Languages-What-are-the-differences-between-Rust-and-Swift)

# Some libraries
## Typed Functional Programming
* Bow: [bow-swift.io](https://bow-swift.io)
## Web frameworks
* [*Kitura*](https://en.m.wikipedia.org/wiki/Kitura)
* [*Vapor (web framework)*](https://en.m.wikipedia.org/wiki/Vapor_(web_framework))
